import model.Circle;

import model.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5.5);
        System.out.println("Dien tich: " + circle.getArea());
        System.out.println("Chu vi: " + circle.getPerimeter());

        ResizableCircle resizableCircle = new ResizableCircle(6.5);
        resizableCircle.resize(10);
        System.out.println("Dien tich: " + resizableCircle.getArea());
        System.out.println("Chu vi: " + resizableCircle.getPerimeter());
    }
}
